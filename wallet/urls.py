from django.urls import path
from wallet.views import transaction_view, transaction_list

urlpatterns = [
    path('transaction/', transaction_view, name='transaction_url'),
    path('transaction_list/', transaction_list, name='transaction_list_url'),
]